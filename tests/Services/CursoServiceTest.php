<?php

use Cslucano\PhpDay2017\Entity\Curso;
use Cslucano\PhpDay2017\Entity\Alumno;
use Cslucano\PhpDay2017\Services\CursoService;

class CursoServiceTest extends \PHPUnit\Framework\TestCase
{
    /** @var  \Cslucano\PhpDay2017\Entity\CursoRepository | PHPUnit_Framework_MockObject_MockObject */
    protected $cursoRepository;

    /** @var  \Cslucano\PhpDay2017\Entity\AlumnoRepository | PHPUnit_Framework_MockObject_MockObject*/
    protected $alumnoRepository;

    /** @var  CursoService */
    protected $SUT;

    /** @var  Curso */
    protected $curso;

    /** @var  Alumno */
    protected $alumno;

    protected function setUp()
    {
        $this->cursoRepository = $this->createMock('\Cslucano\PhpDay2017\Entity\CursoRepository');
        $this->alumnoRepository = $this->createMock('\Cslucano\PhpDay2017\Entity\AlumnoRepository');

        $this->curso = new Curso();
        $this->curso->setCodigo("ABC");

        $this->alumno = new Alumno();
        $this->alumno->setCodigo('z');

        $this->SUT = new CursoService($this->cursoRepository, $this->alumnoRepository);
    }

    public function testTrue()
    {
        $this->assertTrue(true);
    }

    public function testInscribirAlumnoCursoVacio()
    {
        $this->cursoRepository
            ->expects($this->any())
            ->method('find')
            ->with(12)
            ->will($this->returnValue($this->curso));

        $this->SUT->inscribirAlumno(12, 34);
    }


    /**
     * @group now
     */

    public function testInscribirAlumnoUltimoAlumno()
    {
        $alumno1 = new \Cslucano\PhpDay2017\Entity\Alumno();
        $alumno1->setCodigo('a');
        $alumno2 = new \Cslucano\PhpDay2017\Entity\Alumno();
        $alumno2->setCodigo('b');
        $this->curso->agregarAlumno($alumno1);
        $this->curso->agregarAlumno($alumno2);
        $this->cursoRepository
            ->expects($this->any())
            ->method('find')
            ->with(12)
            ->will($this->returnValue($this->curso));

        $this->alumnoRepository
            ->expects($this->any())
            ->method('find')
            ->with(34)
            ->will($this->returnValue($this->alumno));

        $this->SUT->inscribirAlumno(12, 34);
    }

    public function testInscribirAlumnoCursoLleno()
    {
        $this->expectException(Exception::class);

        $alumno1 = new \Cslucano\PhpDay2017\Entity\Alumno();
        $alumno1->setCodigo('a');
        $alumno2 = new \Cslucano\PhpDay2017\Entity\Alumno();
        $alumno2->setCodigo('b');
        $alumno3 = new \Cslucano\PhpDay2017\Entity\Alumno();
        $alumno3->setCodigo('c');
        $this->curso->agregarAlumno($alumno1);
        $this->curso->agregarAlumno($alumno2);
        $this->curso->agregarAlumno($alumno3);
        $this->cursoRepository
            ->expects($this->any())
            ->method('find')
            ->with(12)
            ->will($this->returnValue($this->curso));

        $this->SUT->inscribirAlumno(12, 34);
    }
}
