<?php

namespace Cslucano\PhpDay2017\Entity;

class Curso
{
    /** @var string $codigo*/
    protected $codigo;

    /** @var  Alumno[] */
    protected $alumnos;

    /**
     * Curso constructor.
     */
    public function __construct()
    {
        $this->alumnos = [];
    }


    /**
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @param string $codigo
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    /**
     * @return Alumno[]
     */
    public function getAlumnos()
    {
        return $this->alumnos;
    }

    /**
     * @param Alumno $alumnos
     */
    public function agregarAlumno($alumno)
    {
        $this->alumnos[] = $alumno;
    }
}
