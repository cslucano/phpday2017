<?php

namespace Cslucano\PhpDay2017\Entity;

class Alumno
{
    /** @var  string $codigo */
    protected $codigo;
    
    /** @var  string $nombre */
    protected $nombre;

    /**
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @param string $codigo
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }
}
