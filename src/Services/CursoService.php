<?php

namespace Cslucano\PhpDay2017\Services;

use Cslucano\PhpDay2017\Entity\AlumnoRepository;
use Cslucano\PhpDay2017\Entity\CursoRepository;

class CursoService
{
    /** @var CursoRepository  */
    protected $cursoRepository;

    /** @var AlumnoRepository  */
    protected $alumnoRepository;

    public function __construct(CursoRepository $cursoRepository, AlumnoRepository $alumnoRepository)
    {
        $this->cursoRepository = $cursoRepository;
        $this->alumnoRepository = $alumnoRepository;
    }

    public function inscribirAlumno($idCurso, $idAlumno)
    {
        /** @var Curso $curso */
        $curso = $this->cursoRepository->find($idCurso);
        
        $inscritos = $curso->getAlumnos();
        
        $nuevoAlumno = $this->alumnoRepository->find($idAlumno);
        if (count($inscritos) < 3) {
            $matriculado = false;
            foreach ($inscritos as $alumnoinscrito) {
                if ($alumnoinscrito->getCodigo()  == $nuevoAlumno->getCodigo()) {
                    $matriculado = true;
                }
            }

            if (!$matriculado) {
                $curso->agregarAlumno($nuevoAlumno);
                $this->cursoRepository->persist($curso);
            } else {
                throw new \Exception();
            }
        } else {
            throw new \Exception();
        }
    }
}
